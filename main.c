#include "dictionary.h"
#include "arguments.h"


void view_commands();
uint8_t* read_command();


void command_add(dictionary_t* d);
void command_delete(dictionary_t* d);
void command_find(const dictionary_t* d);
void command_show(const dictionary_t* d);


int main(int argc, char* argv[])
{
    parameters_t params = { NULL };

    parse_arguments(argc, argv, &params);

    dictionary_t* d;
    dstatus s;
    /* Choose init mode */
    switch (params.mode)
    {
        case DICT_MODE_INIT:
            s = dictionary_init(params.init_capacity, &d);
            if (s != DICT_STATUS_OK)
            {
                fprintf(stderr, "Error: Dictionary not init\n");
                exit(1);
            }
            printf("Dictionary was init\n");
            break;
        case DICT_MODE_LOAD:
            s = dictionary_load(params.filename, &d);
            if (s != DICT_STATUS_OK)
            {
                fprintf(stderr, "Error: Dictionary not load\n");
                exit(1);
            }
            printf("Dictionary was loaded from file '%s'\n", params.filename);
            break;
        case DICT_MODE_FAIL:
            fprintf(stderr, "Error: Unexpected arguments\n");
        case DICT_MODE_HELP:
            print_help();
        default:
            exit(1);
    }

    uint8_t* command;

    /* User actions with dictionary */
    while (view_commands(), command = read_command() , *command != 'q')
    {
        if (command[1] != '\0')
        {
            printf("Invalid command. Try again\n");
            continue;
        }

        switch (*command)
        {
            case 'a':
                command_add(d);
                break;
            case 'd':
                command_delete(d);
                break;
            case 'f':
                command_find(d);
                break;
            case 's':
                command_show(d);
                break;
            default:
                printf("Invalid command. Try again\n");
                break;
        }
    }

    while ((s = dictionary_save(params.filename, d)), s != DICT_STATUS_OK)
    {
        printf("Can't save dictionary\n"
               "Enter correct filename: ");
        params.filename = dictionary_read_string(stdin, 256);
    }
    printf("File was saved successfully\n");

    dictionary_free(d);
    return 0;
}

void view_commands()
{
    printf("\nCommands:\n"
                   "\ta - add word to dictionary\n"
                   "\td - delete word from dictionary\n"
                   "\tf - find definition of word in dictionary\n"
                   "\ts - show available words\n"
                   "\tq - quit\n");
}

uint8_t* read_command()
{
    printf(">>> ");
    return dictionary_read_string(stdin, 2);
}

void command_add(dictionary_t* d)
{
    printf("Enter word: ");
    uint8_t* word       = dictionary_read_string(stdin, DICT_WORD_SIZE + 1);

    printf("Enter definition: ");
    uint8_t* definition = dictionary_read_string(stdin, 256);

    dstatus s = dictionary_add(d, word, definition);
    if (s == DICT_STATUS_ERROR_WORD_SIZE)
    {
        printf("Word must be less 128 characters\n\n");
        return;
    }

    printf("Word was added successfully\n\n");
}

void command_delete(dictionary_t* d)
{
    printf("Enter word to delete: ");
    uint8_t* word = dictionary_read_string(stdin, DICT_WORD_SIZE);

    dstatus s = dictionary_delete(d, word);
    if (s == DICT_STATUS_NOT_FOUND)
    {
        printf("Word '%s' not added\n", word);
        return;
    }

    printf("Word was deleted successfully\n");
}

void command_find(const dictionary_t* d)
{
    printf("Enter word to find: ");
    uint8_t* word = dictionary_read_string(stdin, DICT_WORD_SIZE);

    uint8_t* def = dictionary_search(d, word);

    if (def == NULL)
    {
        printf("Word '%s' not added\n", word);
        return;
    }

    printf("%s\n", def);
}

void command_show(const dictionary_t* dict)
{
    printf("\nNow in dictionary stored %ld words\n\n", dict->size);

    uint8_t* array[dict->size];
    uint8_t** head = array;

    dictionary_traverse(dict->table, dict->capacity, LAMBDA(void _(entry_t* e)
    {
        *head++ = e->word;
    }
    ));

    qsort(array, dict->size, sizeof(uint8_t*), LAMBDA(int _(const void* s1, const void* s2)
    {
        return strcmp(*(const char **)s1, *(const char **)s2);
    }
    ));

    for (int i = 0; i < dict->size; ++i)
    {
        printf("%s\n", array[i]);
    }
}
