#Compiler
CC=gcc

#Flags
CFLAGS=-c -Wall


all: dictionary

dictionary: main.o dictionary.o
		$(CC) main.o dictionary.o -o dictionary

main.o: main.c
		$(CC) $(CFLAGS) main.c

dictionary.o: dictionary.c
		$(CC) $(CFLAGS) dictionary.c

clean:
		rm -rf *.o dictionary

