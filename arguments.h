//
// Created by rzhevskiy on 10/2/17.
//

#pragma once

#include <getopt.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>


struct parameters_s
{
    uint8_t*    filename;
    int64_t     init_capacity;
    dmode       mode;
};

typedef struct parameters_s parameters_t;

/**
 * Print help message
 *
 * */
void print_help()
{
    printf("Usage: dictionary [-f FILENAME]\n\n"
           "With no FILENAME create new dictionary in memory.\n\n"
           "Options:\n"
           "\t-f, --file    \n\t\tLoad existing dictionary from file\n"
           "\t--help        \n\t\tShow help\n");
}

/**
 * Short options for command-line
 *
 * */
const char* const short_options = ":f:";

/**
 * Long options for command-line
 * */
static struct option long_options[] = {
        {"file",    required_argument,  NULL,  'f'},
        {"help",    no_argument,        NULL,   1 },
        {NULL,      0,                  NULL,   0 }
};

void parse_arguments(int argc, char* argv[], parameters_t* params)
{
    int c = 0;
    while ((c = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
    {
        switch (c)
        {
            case 'f':
                params->mode     = DICT_MODE_LOAD;
                params->filename = (uint8_t* )strdup(optarg);
                break;
            default:
                params->mode = DICT_MODE_HELP;
                params->filename = NULL;
                break;
        }
    }

    if (optind < argc)
    {
        params->mode = DICT_MODE_FAIL;
        params->filename = NULL;
    }
    else if (params->filename == NULL)
    {
        params->mode          = DICT_MODE_INIT;
        params->init_capacity = 64;
        params->filename = NULL;
    }
}
