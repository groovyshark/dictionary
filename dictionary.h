//
// Created by rzhevskiy on 9/29/17.
//

#pragma once

#include <stdio.h>
#include <stdint.h>


#define DICT_WORD_SIZE 128

#define LAMBDA(c_) ({ c_ _;})
#define SKIP_NEWLINE(file) fscanf(file, "%*[\n]\n")

/* Return codes */
enum dict_status
{
    DICT_STATUS_NOT_FOUND = -1,
    DICT_STATUS_OK,
    DICT_STATUS_ERROR_WORD_SIZE,
    DICT_STATUS_ERROR_OPEN_FILE,
    DICT_STATUS_ERROR_MEMORY,
    DICT_STATUS_ERROR_NEGATIVE_CAPACITY
};

typedef enum dict_status dstatus;

enum dict_modes
{
    DICT_MODE_FAIL = -1,
    DICT_MODE_HELP,
    DICT_MODE_INIT,
    DICT_MODE_LOAD
};

typedef enum dict_modes dmode;

struct entry_s
{
    uint8_t* word;
    uint8_t* definition;

    struct entry_s *next;
};

typedef struct entry_s entry_t;


struct dictionary_s
{
    struct entry_s** table;

    int64_t capacity;
    int64_t size;
};

typedef struct dictionary_s dictionary_t;


/**
 * Initialize new dictionary
 *
 * @param capacity capacity of new dictionary
 *
 * @return new dictionary
 *
 * */
dstatus dictionary_init (int64_t capacity, dictionary_t** dest);

/**
 * Free memory for dictionary
 *
 * @param dict source dictionary
 *
 * */
void          dictionary_free (dictionary_t* dict);

/**
 * Generate hash
 *
 * @param dict source dictionary
 * @param word word
 *
 * @return hash
 *
 * */
int64_t       dictionary_hash(const dictionary_t* dict, uint8_t* word);

/**
 * Create new entry from pair word-definition
 *
 * @param word new word
 * @param definition definition of new word
 *
 * @return new entry
 *
 * */
entry_t*      dictionary_new_entry(uint8_t* word, uint8_t* definition);

/**
 * Free memory for entry
 *
 * @param entry entry
 *
 * */
void          dictionary_free_entry(entry_t* entry);

/**
 * Dictionary traverse
 *
 * @param table pointer on table-data of dictionary
 * @param capacity dictionary capacity
 * @param callback pointer on callback-function
 *
 * */
void          dictionary_traverse(entry_t** const table, const int64_t capacity, void (*callback)(entry_t*));

/**
 * Add new word in dictionary. If word is already exists,
 *
 * @param dict source dictionary
 * @param word new word
 * @param definition definition of new word
 *
 * @return OK, if added was successfully
 *         ERROR_WORD_SIZE, if word size more 128
 *
 * */
dstatus       dictionary_add(dictionary_t* dict, uint8_t* word, uint8_t* definition);

/**
 * Delete word from dictionary
 *
 * @param dict source dictionary
 * @param word word for delete
 *
 * return OK, if word was deleted
 *        NOT_FOUND, if word not exist
 *
 * */
dstatus        dictionary_delete(dictionary_t* dict, uint8_t* word);

/**
 * Save dictionary to file
 *
 * @param path path to file
 * @param dict source dictionary
 *
 * return OK, if file successfully saved
 *        ERROR_OPEN_FILE, if can't open file
 *
 * */
dstatus        dictionary_save(const uint8_t* path, const dictionary_t* dict);

/**
 * Read string until newline character from file
 *
 * @param file pointer to opened file
 * @param size start size of string
 *
 * @return string
 *
 * */
uint8_t* dictionary_read_string(FILE* file, size_t size);

/**
 * Load dictionary from file
 *
 * @param path path to file
 *
 * @return loaded dictionary
 * */
dstatus dictionary_load(const uint8_t* path, dictionary_t** dest);

/**
 * Search definition of word in dictionary
 *
 * @param dict source dictionary
 * @param word word
 *
 * @return definition of word, if word added
 *         NULL, if word not exists
 *
 * */
uint8_t*      dictionary_search(const dictionary_t* dict, uint8_t* word);
