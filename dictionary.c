//
// Created by rzhevskiy on 9/29/17.
//

#include <stddef.h>
#include <malloc.h>
#include <string.h>
#include "dictionary.h"
#include <sys/stat.h>


void dictionary_traverse(entry_t** const table, const int64_t capacity, void (*callback)(entry_t*))
{
    entry_t** head = table;
    for (int i = 0; i < capacity; ++i)
    {
        entry_t* entry = *head++;
        while (entry != NULL)
        {
            entry_t* next = entry->next;

            callback(entry);

            entry = next;
        }
    }
}

dstatus dictionary_init(int64_t capacity, dictionary_t** dest)
{
    if (capacity < 0)
    {
        return DICT_STATUS_ERROR_NEGATIVE_CAPACITY;
    }

    dictionary_t* d = malloc(sizeof(dictionary_t));
    if (d == NULL)
    {
        return DICT_STATUS_ERROR_MEMORY;
    }

    d->table = calloc(capacity, sizeof(entry_t));
    if (d->table == NULL)
    {
        free(d);
        return DICT_STATUS_ERROR_MEMORY;
    }

    d->capacity = capacity;
    d->size = 0;

    *dest = d;

    return DICT_STATUS_OK;
}


void dictionary_free(dictionary_t* dict)
{
    dictionary_traverse(dict->table, dict->capacity, LAMBDA(void _(entry_t* e)
    {
        dictionary_free_entry(e);
    }
    ));

    free(dict->table);
    free(dict);
}


int64_t dictionary_hash(const dictionary_t* dict, uint8_t* word)
{
    uint64_t hash_value = 0;

    size_t i = 0;
    while (i < strlen((char*)word))
    {
        hash_value = hash_value << 8;
        hash_value += word[i];
        ++i;
    }

    return hash_value % dict->capacity;
}

entry_t* dictionary_new_entry(uint8_t* word, uint8_t* definition)
{
    entry_t* entry = malloc(sizeof(entry_t));

    if (entry == NULL)
    {
        return NULL;
    }

    entry->word       = (uint8_t*) strdup((char*)word);
    entry->definition = (uint8_t*) strdup((char*)definition);
    if (entry->word == NULL || entry->definition == NULL)
    {
        return NULL;
    }

    entry->next = NULL;

    return entry;
}


void dictionary_free_entry(entry_t* entry)
{
    free(entry->word);
    free(entry->definition);
    free(entry);
}

dstatus dictionary_resize(dictionary_t* dict)
{
    entry_t** old_table    = dict->table;
    int64_t   old_capacity = dict->capacity;

    int64_t new_capacity = old_capacity * 2;

    entry_t** new_table = calloc((size_t)new_capacity, sizeof(entry_t));
    if (new_table == NULL)
    {
        return DICT_STATUS_ERROR_MEMORY;
    }

    dict->table = new_table;
    dict->capacity = new_capacity;
    dict->size = 0;

    dictionary_traverse(old_table, old_capacity, LAMBDA(void _(entry_t* e)
    {
        dictionary_add(dict, e->word, e->definition);
        dictionary_free_entry(e);
    }
    ));

    free(old_table);
    return DICT_STATUS_OK;
}

dstatus dictionary_add(dictionary_t* dict, uint8_t* word, uint8_t* definition)
{
    if (strlen((char*)word) > 128)
    {
        return DICT_STATUS_ERROR_WORD_SIZE;
    }

    int64_t hash = dictionary_hash(dict, word);

    entry_t* next = dict->table[hash];

    entry_t* last = NULL;
    while (next != NULL && next->word != NULL && strcmp((char*)word, (char*)next->word) > 0)
    {
        last = next;
        next = next->next;
    }

    if (next != NULL && next->word != NULL && strcmp((char*)word, (char*)next->word) == 0)
    {
        free(next->definition);
        next->definition = (uint8_t*)strdup((char*)definition);

        return DICT_STATUS_OK;
    }
    else
    {
        entry_t* entry = dictionary_new_entry(word, definition);

        if (next == dict->table[hash])
        {
            entry->next = next;
            dict->table[hash] = entry;
        }
        else if (next == NULL)
        {
            last->next = entry;
        }
        else
        {
            entry->next = next;
            last->next = entry;
        }

        dict->size++;

        if (dict->size > dict->capacity / 2)
        {
            dictionary_resize(dict);
        }
    }

    return DICT_STATUS_OK;
}

dstatus dictionary_delete(dictionary_t* dict, uint8_t* word)
{
    int64_t hash = dictionary_hash(dict, word);

    entry_t* entry = dict->table[hash];

    if (entry == NULL)
    {
        return DICT_STATUS_NOT_FOUND;
    }
    else if ( strcmp((char*)entry->word, (char*)word) == 0 )
    {
        dict->table[hash] = entry->next;

        dictionary_free_entry(entry);

        dict->size--;
        return DICT_STATUS_OK;
    }

    while (entry != NULL)
    {
        if (entry->next != NULL)
        {
            if ( strcmp((char*)entry->next->word, (char*)word) == 0 )
            {
                entry->next = entry->next->next;
                dictionary_free_entry(entry->next);

                dict->size--;

            }
            entry = entry->next;
        }
        else
        {
            return DICT_STATUS_NOT_FOUND;
        }
    }

    return DICT_STATUS_OK;
}

uint8_t* dictionary_search(const dictionary_t* dict, uint8_t* word)
{
    int64_t hash = dictionary_hash(dict, word);

    entry_t* entry = dict->table[hash];

    while (entry != NULL)
    {
        if (strcmp((char*)entry->word, (char*)word) == 0)
        {
            return entry->definition;
        }

        entry = entry->next;
    }

    return NULL;
}

dstatus dictionary_save(const uint8_t* path, const dictionary_t* dict)
{
    FILE* file = fopen((char*)path, "w");
    if ( file == NULL )
    {
        return DICT_STATUS_ERROR_OPEN_FILE;
    }

    fprintf(file, "%ld\n\n", dict->size);

    dictionary_traverse(dict->table, dict->capacity, LAMBDA(void _(entry_t* e)
    {
        fprintf(file, "%s\n%s\n\n", e->word, e->definition);
    }
    ));

    return DICT_STATUS_OK;
}

uint8_t* dictionary_read_string(FILE* file, size_t size)
{
    uint8_t ch;
    size_t len = 0;

    uint8_t* def = malloc(size);
    while( ch = (uint8_t)fgetc(file), ch != EOF && ch != '\n' )
    {
        def[len++] = ch;
        if( len == size )
        {
            size *= 2;
            def = realloc(def, size);
        }
    }
    def[len++] = '\0';

    return realloc(def, len);
}

dstatus dictionary_load(const uint8_t* path, dictionary_t** dest)
{
    FILE* file = fopen((char*)path, "r");
    if ( file == NULL )
    {
        return DICT_STATUS_ERROR_OPEN_FILE;
    }

    int64_t size = 0;
    fscanf(file, "%ld", &size);
    SKIP_NEWLINE(file);

    dictionary_t* dict;
    dictionary_init(size * 2 + 1, &dict);

    if (size != 0)
    {
        struct stat st;
        fstat(fileno(file), &st);

        size_t def_size = (size_t)(st.st_size / size);

        uint8_t* word;
        uint8_t* definition;
        while (!feof(file))
        {
            word = dictionary_read_string(file, 128);
            SKIP_NEWLINE(file);

            definition = dictionary_read_string(file, def_size);
            SKIP_NEWLINE(file);

            dictionary_add(dict, word, definition);
        }
        fclose(file);
    }

    *dest = dict;

    return DICT_STATUS_OK;
}
